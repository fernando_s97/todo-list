import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class BasePage extends StatelessWidget {
  final List<SingleChildWidget>? _bindings;
  final WidgetBuilder _builder;

  const BasePage({
    Key? key,
    List<SingleChildWidget>? bindings,
    required WidgetBuilder builder,
  })  : _bindings = bindings,
        _builder = builder,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    if (_bindings == null || _bindings!.isEmpty) return _builder(context);

    return MultiProvider(
      providers: _bindings!,
      child: Builder(builder: (context) => _builder(context)),
    );
  }
}
