# App

This app is a simple todo list. You can:

- Sign up/in with Google or email with password
- Change the display name of your account
- Create tasks
- Delete tasks
- Mark tasks as done or undone
- Filter them by tasks of the day, tasks of tomorrow or tasks for a specific day of the current week
- Filter only undone tasks

OBS 1: The login methods are not interchangeable. If you signed up with google, you must also sign
in with Google.

OBS 2: All tasks are stored locally and will be erased in logout.

# Goals

This project is basically to learn some things:

- [x] Architecture in modules
- [x] SQLite
- [x] Change the launcher icon properly
- [x] Add splash screen properly
- [x] Make authentication with email and password using Firebase
- [x] Make authentication with Google

Also, it has two branches. The `main` one uses `ChangeNotifier` with `Provider` for state management.
The `mobx` one, well, uses `MobX` for state management.

The reason for having those two branches were to learn these two techniques of state management and 
having a better understanding in the impacts of replacing one state management with another one. 
This was also a good test to verify if the architecture had its responsibility well separated.