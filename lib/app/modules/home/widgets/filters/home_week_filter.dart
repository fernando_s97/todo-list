import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/date_time_extension.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/models/task_filter.dart';
import 'package:todo/app/modules/home/home_controller.dart';

class HomeWeekFilter extends StatelessWidget {
  late final DateTime _currentDateTime;
  final HomeController _homeController;

  HomeWeekFilter({Key? key, required HomeController homeController})
      : _homeController = homeController,
        super(key: key) {
    _currentDateTime = DateTime.now().getMostRecentMonday();
  }

  @override
  Widget build(BuildContext context) {
    return Selector<HomeController, TaskFilter>(
      selector: (context, controller) => controller.selectedFilter,
      builder: (context, selectedFilter, child) {
        if (selectedFilter != TaskFilter.week) return const SizedBox();

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('WEEK DAY', style: context.titleStyle),
            const SizedBox(height: 10),
            SizedBox(
              height: 95,
              child: Selector<HomeController, DateTime>(
                selector: (context, controller) {
                  return controller.selectedDateForWeekFilter!;
                },
                builder: (context, selectedDate, child) {
                  return DatePicker(
                    _currentDateTime,
                    daysCount: 7,
                    initialSelectedDate: selectedDate,
                    selectionColor: context.primaryColor,
                    monthTextStyle: const TextStyle(fontSize: 8),
                    dayTextStyle: const TextStyle(fontSize: 13),
                    dateTextStyle: const TextStyle(fontSize: 13),
                    locale: Localizations.localeOf(context).toString(),
                    onDateChange: _onSelectedDateChanged,
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }

  // ignore:use_setters_to_change_properties
  void _onSelectedDateChanged(DateTime selectedDate) {
    _homeController.selectedDateForWeekFilter = selectedDate;
  }
}
