import 'package:flutter/material.dart';

class CustomNavigator {
  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();

  static NavigatorState get it => navigatorKey.currentState!;
}
