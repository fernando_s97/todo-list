import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UiConfig {
  static ThemeData get theme {
    const primaryColor = Color(0xff5c77ce);
    const appBarIconThemeData = IconThemeData(color: primaryColor);
    const appBarAndCanvasColor = Color(0xfffafbfe);

    final baseTheme = ThemeData.light();

    return baseTheme.copyWith(
      appBarTheme: const AppBarTheme(
        iconTheme: appBarIconThemeData,
        actionsIconTheme: appBarIconThemeData,
        backgroundColor: appBarAndCanvasColor,
        elevation: 0,
      ),
      canvasColor: appBarAndCanvasColor,
      textTheme: GoogleFonts.mandaliTextTheme(),
      primaryColor: primaryColor,
      primaryColorLight: const Color(0xffabc8f7),
      colorScheme: baseTheme.colorScheme.copyWith(
        primary: primaryColor,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          padding: const EdgeInsets.symmetric(
            horizontal: 25,
            vertical: 10,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
      floatingActionButtonTheme: baseTheme.floatingActionButtonTheme.copyWith(
        backgroundColor: primaryColor,
      ),
    );
  }

  UiConfig._();
}
