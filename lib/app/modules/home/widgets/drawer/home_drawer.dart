import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/modules/home/widgets/drawer/change_name_dialog.dart';
import 'package:todo/app/modules/home/widgets/drawer/home_drawer_controller.dart';

class HomeDrawer extends StatelessWidget {
  static const String _avatarPlaceholderUrl =
      'https://toppng.com/uploads/preview/roger-berry-avatar-placeholder-11562991561rbrfzlng6h.png';
  static const String _userNamePlaceholder = '-';

  final HomeDrawerController _homeDrawerController;

  const HomeDrawer({
    Key? key,
    required HomeDrawerController homeDrawerController,
  })  : _homeDrawerController = homeDrawerController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: context.primaryColor.withAlpha(70),
            ),
            child: Row(
              children: [
                Selector<HomeDrawerController, User?>(
                  selector: (context, controller) => controller.currentUser,
                  builder: (context, user, child) => CircleAvatar(
                    backgroundImage: NetworkImage(
                      user?.photoURL ?? HomeDrawer._avatarPlaceholderUrl,
                    ),
                    radius: 30,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Selector<HomeDrawerController, User?>(
                      selector: (context, controller) => controller.currentUser,
                      builder: (context, user, child) => Text(
                        user?.displayName ?? HomeDrawer._userNamePlaceholder,
                        style: context.textTheme.subtitle2,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            title: const Text('Change name'),
            onTap: () => _onChangeNameClicked(context),
          ),
          ListTile(
            title: const Text('Sign out'),
            onTap: _onSignOutClicked,
          ),
        ],
      ),
    );
  }

  void _onChangeNameClicked(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => ChangeNameDialog(
        homeDrawerController: _homeDrawerController,
      ),
    );
  }

  void _onSignOutClicked() => _homeDrawerController.signOut();
}
