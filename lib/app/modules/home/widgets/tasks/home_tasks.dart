import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/task_filter_extension.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/models/task_filter.dart';
import 'package:todo/app/models/task_model.dart';
import 'package:todo/app/modules/home/home_controller.dart';
import 'package:todo/app/modules/home/widgets/tasks/task_widget.dart';

class HomeTasks extends StatelessWidget {
  final HomeController _homeController;

  const HomeTasks({Key? key, required HomeController homeController})
      : _homeController = homeController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Selector<HomeController, TaskFilter>(
          selector: (context, controller) => controller.selectedFilter,
          builder: (context, filter, child) {
            final filterDescription = filter.description.toUpperCase();
            return Text(
              'TASKS OF $filterDescription',
              style: context.titleStyle,
            );
          },
        ),
        const SizedBox(height: 10),
        Expanded(
          child: Selector<HomeController, List<TaskModel>>(
            selector: (context, controller) => controller.tasksForCurrentFilter,
            builder: (context, tasks, child) {
              if (tasks.isEmpty) {
                return SvgPicture.asset('assets/images/no_tasks.svg');
              }

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: tasks.mapIndexed((index, task) {
                  final isLastElement = index == tasks.length - 1;

                  return Column(
                    children: [
                      TaskWidget(
                        task: task,
                        onDoneChanged: (isDone) => _onDoneChanged(task),
                        onDeleteClicked: () => _onDeleteClicked(task),
                      ),
                      if (!isLastElement) const SizedBox(height: 10),
                    ],
                  );
                }).toList(),
              );
            },
          ),
        ),
      ],
    );
  }

  void _onDoneChanged(TaskModel task) {
    _homeController.toggleTaskDoneState(task);
  }

  void _onDeleteClicked(TaskModel task) {
    _homeController.deleteTask(task);
  }
}
