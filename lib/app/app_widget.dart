import 'dart:async';

import 'package:flutter/material.dart';
import 'package:todo/app/core/database/sqlite_connection_observer.dart';
import 'package:todo/app/core/helpers/custom_navigator.dart';
import 'package:todo/app/core/ui/ui_config.dart';
import 'package:todo/app/modules/auth/auth_module.dart';
import 'package:todo/app/modules/auth/sign_in/sign_in_page.dart';
import 'package:todo/app/modules/home/home_module.dart';
import 'package:todo/app/modules/home/home_page.dart';
import 'package:todo/app/modules/tasks/tasks_module.dart';
import 'package:todo/app/services/user/user_service.dart';

class AppWidget extends StatefulWidget {
  final UserService _userService;

  const AppWidget({Key? key, required UserService userService})
      : _userService = userService,
        super(key: key);

  @override
  State<AppWidget> createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  final _sqliteConnectionObserver = SqliteConnectionObserver();
  late final StreamSubscription<bool> _signedInStateSubscription;

  @override
  void initState() {
    super.initState();

    _signedInStateSubscription =
        widget._userService.signedInStateStream.listen(_onSignedInStateChanged);
    WidgetsBinding.instance!.addObserver(_sqliteConnectionObserver);
  }

  void _onSignedInStateChanged(bool isSignedIn) {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      final newRoute = isSignedIn ? HomePage.routeName : SignInPage.routeName;
      CustomNavigator.it.pushNamedAndRemoveUntil(newRoute, (route) => false);
    });
  }

  @override
  void dispose() {
    _signedInStateSubscription.cancel();
    WidgetsBinding.instance!.removeObserver(_sqliteConnectionObserver);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Todo List',
      theme: UiConfig.theme,
      navigatorKey: CustomNavigator.navigatorKey,
      initialRoute: SignInPage.routeName,
      routes: {
        ...AuthModule().routes,
        ...HomeModule().routes,
        ...TasksModule().routes,
      },
    );
  }
}
