import 'package:provider/provider.dart';
import 'package:todo/app/core/modules/base_module.dart';
import 'package:todo/app/modules/home/home_controller.dart';
import 'package:todo/app/modules/home/home_page.dart';
import 'package:todo/app/modules/home/widgets/drawer/home_drawer_controller.dart';
import 'package:todo/app/modules/home/widgets/header/home_header_controller.dart';

class HomeModule extends BaseModule {
  HomeModule()
      : super(
          bindings: [
            ChangeNotifierProvider(
              create: (context) => HomeController(
                tasksService: context.read(),
              ),
            ),
            ChangeNotifierProvider(
              create: (context) => HomeDrawerController(
                userService: context.read(),
                tasksService: context.read(),
              ),
            ),
            ChangeNotifierProvider(
              create: (context) => HomeHeaderController(
                userService: context.read(),
              ),
            ),
          ],
          routes: {
            HomePage.routeName: (context) {
              return HomePage(
                homeController: context.read(),
              );
            },
          },
        );
}
