import 'package:firebase_auth/firebase_auth.dart';

abstract class UserService {
  User? get currentUser;

  Stream<User?> get currentUserStream;

  Stream<bool> get signedInStateStream;

  Future<User?> signUp({required String email, required String password});

  Future<User?> signInWithEmailAndPassword({
    required String email,
    required String password,
  });

  Future<void> recoverPassword({required String email});

  Future<User?> signInWithGoogle();

  Future<void> signOut();

  Future<void> changeUserName(String newName);
}
