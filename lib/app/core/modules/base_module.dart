import 'package:flutter/material.dart';
import 'package:provider/single_child_widget.dart';
import 'package:todo/app/core/modules/base_page.dart';

abstract class BaseModule {
  final List<SingleChildWidget>? _bindings;
  final Map<String, WidgetBuilder> _routes;

  const BaseModule({
    List<SingleChildWidget>? bindings,
    required Map<String, WidgetBuilder> routes,
  })  : _bindings = bindings,
        _routes = routes;

  Map<String, WidgetBuilder> get routes {
    return _routes.map((route, builder) {
      return MapEntry(
        route,
        (_) => BasePage(bindings: _bindings, builder: builder),
      );
    });
  }
}
