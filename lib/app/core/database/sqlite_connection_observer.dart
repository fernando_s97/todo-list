import 'package:flutter/material.dart';
import 'package:todo/app/core/database/sqlite_connection_factory.dart';

class SqliteConnectionObserver with WidgetsBindingObserver {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        SqliteConnectionFactory.instance.closeConnection();
        break;
    }
  }
}
