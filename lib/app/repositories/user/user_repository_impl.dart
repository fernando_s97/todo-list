import 'dart:developer' as dev;

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:todo/app/core/exceptions/auth_exception.dart';
import 'package:todo/app/repositories/user/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;

  UserRepositoryImpl({
    required FirebaseAuth firebaseAuth,
    required GoogleSignIn googleSignIn,
  })  : _firebaseAuth = firebaseAuth,
        _googleSignIn = googleSignIn;

  @override
  User? get currentUser => _firebaseAuth.currentUser;

  @override
  Stream<User?> get currentUserStream => _firebaseAuth.userChanges();

  @override
  Stream<bool> get signedInStateStream =>
      _firebaseAuth.authStateChanges().map((user) => user != null);

  @override
  Future<User?> signUp({
    required String email,
    required String password,
  }) async {
    final UserCredential userCredential;

    try {
      userCredential = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      if (e is FirebaseAuthException) {
        if (e.code == 'email-already-in-use') {
          final signInMethods =
              await _firebaseAuth.fetchSignInMethodsForEmail(email);
          final isRegisteredWithGoogle = signInMethods.contains('google');
          final message = isRegisteredWithGoogle
              ? 'You are already registered through Google. Use this '
                  'authentication method.'
              : e.message ?? 'Failed to sign up.';
          throw AuthException(message);
        }
      }

      throw AuthException(e.toString());
    }

    return userCredential.user;
  }

  @override
  Future<User?> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    final UserCredential userCredential;

    try {
      userCredential = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      if (e is FirebaseAuthException) {
        throw AuthException(e.message ?? 'Failed to sign in.');
      }

      throw AuthException(e.toString());
    }

    return userCredential.user;
  }

  @override
  Future<void> recoverPassword({required String email}) async {
    final signInMethods = await _firebaseAuth.fetchSignInMethodsForEmail(email);
    final isRegisteredWithEmailAndPassword = signInMethods.contains('password');
    if (!isRegisteredWithEmailAndPassword) {
      throw const AuthException(
        'The password can only be redefined for accounts created through email '
        'and password',
      );
    }

    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      throw AuthException(e.message ?? 'Failed to recover password.');
    }
  }

  @override
  Future<User?> signInWithGoogle() async {
    final googleUser = await _googleSignIn.signIn();
    if (googleUser == null) return null;

    final signInMethods =
        await _firebaseAuth.fetchSignInMethodsForEmail(googleUser.email);
    final isRegisteredWithEmailAndPassword = signInMethods.contains('password');
    if (isRegisteredWithEmailAndPassword) {
      throw const AuthException(
        "You're registered with email and password. Use this authentication "
        "method.",
      );
    }

    final googleAuthentication = await googleUser.authentication;
    final googleCredential = GoogleAuthProvider.credential(
      idToken: googleAuthentication.idToken,
      accessToken: googleAuthentication.accessToken,
    );

    final UserCredential firebaseCredential;

    try {
      firebaseCredential =
          await _firebaseAuth.signInWithCredential(googleCredential);
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      if (e is FirebaseAuthException) {
        if (e.code == 'account-exists-with-different-credentials') {
          final signInMethodsDescription = signInMethods.join(',');
          throw AuthException(
            "You're registered through $signInMethodsDescription.",
          );
        }

        throw AuthException(e.message ?? 'Failed to sign in.');
      }

      throw AuthException(e.toString());
    }

    return firebaseCredential.user;
  }

  @override
  Future<void> signOut() async {
    await _firebaseAuth.signOut();
    await _googleSignIn.signOut();
  }

  @override
  Future<void> changeUserName(String newName) async {
    final user = currentUser;

    if (user == null) return;

    await user.updateDisplayName(newName);
  }
}
