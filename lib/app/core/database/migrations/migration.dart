import 'package:sqflite/sqflite.dart';

abstract class Migration {
  Future<void> onCreate(Batch batch);

  Future<void> onUpgrade(Batch batch);
}
