import 'package:flutter/material.dart';

class CustomIcons {
  static const IconData visibility = Icons.visibility;
  static const IconData visibilityOff = Icons.visibility_off;
  static const IconData back = Icons.arrow_back_ios_outlined;
  static const IconData filter = Icons.filter_alt;
  static const IconData add = Icons.add;
  static const IconData today = Icons.today;
  static const IconData trash = Icons.delete_forever;

  const CustomIcons._();
}
