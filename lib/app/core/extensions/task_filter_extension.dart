import 'package:todo/app/models/task_filter.dart';

extension TaskFilterExtension on TaskFilter {
  String get description {
    switch (this) {
      case TaskFilter.today:
        return 'today';
      case TaskFilter.tomorrow:
        return 'tomorrow';
      case TaskFilter.week:
        return 'week';
    }
  }
}
