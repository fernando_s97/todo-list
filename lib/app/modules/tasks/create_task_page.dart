import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/core/helpers/custom_navigator.dart';
import 'package:todo/app/core/widgets/custom_text_form_field.dart';
import 'package:todo/app/modules/tasks/create_task_controller.dart';
import 'package:todo/app/modules/tasks/widgets/due_date_button.dart';

class CreateTaskPage extends StatefulWidget {
  static const String routeName = '/createTask';
  static const double _distanceBetweenWidgets = 20;

  final CreateTaskController _createTaskController;

  const CreateTaskPage({
    Key? key,
    required CreateTaskController createTaskController,
  })  : _createTaskController = createTaskController,
        super(key: key);

  @override
  State<CreateTaskPage> createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<CreateTaskPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  @override
  void initState() {
    super.initState();

    widget._createTaskController.addListener(_onControllerStateChanged);
  }

  void _onControllerStateChanged() {
    final shouldShowLoadingOverlay =
        widget._createTaskController.isCreatingTask;
    if (shouldShowLoadingOverlay) {
      Loader.show(context);
    } else {
      Loader.hide();
    }

    final createTaskResult = widget._createTaskController.createTaskResult;
    if (createTaskResult != null && createTaskResult) {
      CustomNavigator.it.pop();
    }
  }

  @override
  void dispose() {
    widget._createTaskController.removeListener(_onControllerStateChanged);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: const [
          CloseButton(color: Colors.black),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'New tasks',
              style: context.titleStyle.copyWith(fontSize: 20),
            ),
            const SizedBox(height: CreateTaskPage._distanceBetweenWidgets),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomTextFormField(
                    label: 'Description',
                    onChanged: _onDescriptionChanged,
                    validator: _validateDescription,
                  ),
                  const SizedBox(
                    height: CreateTaskPage._distanceBetweenWidgets,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: DueDateButton(
                      createTaskController: widget._createTaskController,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _onCreateTaskClicked,
        label: const Text(
          'Create',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  // ignore:use_setters_to_change_properties
  void _onDescriptionChanged(String description) {
    widget._createTaskController.description = description;
  }

  String? _validateDescription(String? description) {
    return widget._createTaskController.validateDescription(description);
  }

  void _onCreateTaskClicked() {
    final isFormValid = _formKey.currentState!.validate();

    if (!isFormValid) return;

    widget._createTaskController.create();
  }
}
