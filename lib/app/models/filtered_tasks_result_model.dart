import 'package:todo/app/core/helpers/utils.dart';
import 'package:todo/app/models/task_model.dart';

class FilteredTasksResultModel {
  final DateTime from;
  final DateTime upTo;
  final List<TaskModel> tasks;

  const FilteredTasksResultModel({
    required this.from,
    required this.upTo,
    required this.tasks,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FilteredTasksResultModel &&
          Utils.equality.equals(runtimeType, other.runtimeType) &&
          Utils.equality.equals(from, other.from) &&
          Utils.equality.equals(upTo, other.upTo) &&
          Utils.equality.equals(tasks, other.tasks));

  @override
  int get hashCode => from.hashCode ^ upTo.hashCode ^ tasks.hashCode;

  @override
  String toString() {
    return 'FilteredTasksResultModel(from: $from, upTo: $upTo, tasks: $tasks)';
  }
}
