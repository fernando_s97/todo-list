import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo/app/repositories/user/user_repository.dart';
import 'package:todo/app/services/user/user_service.dart';

class UserServiceImpl implements UserService {
  final UserRepository _userRepository;

  UserServiceImpl({required UserRepository userRepository})
      : _userRepository = userRepository;

  @override
  User? get currentUser => _userRepository.currentUser;

  @override
  Stream<User?> get currentUserStream => _userRepository.currentUserStream;

  @override
  Stream<bool> get signedInStateStream => _userRepository.signedInStateStream;

  @override
  Future<User?> signUp({required String email, required String password}) {
    return _userRepository.signUp(email: email, password: password);
  }

  @override
  Future<User?> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) {
    return _userRepository.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  @override
  Future<void> recoverPassword({required String email}) {
    return _userRepository.recoverPassword(email: email);
  }

  @override
  Future<User?> signInWithGoogle() {
    return _userRepository.signInWithGoogle();
  }

  @override
  Future<void> signOut() {
    return _userRepository.signOut();
  }

  @override
  Future<void> changeUserName(String newName) {
    return _userRepository.changeUserName(newName);
  }
}
