import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/modules/home/widgets/header/home_header_controller.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<HomeHeaderController, User?>(
      selector: (context, controller) => controller.currentUser,
      builder: (context, user, child) {
        final name = user?.displayName ?? 'User';

        return Text(
          'Hey, $name!',
          style: context.textTheme.headline5!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );
  }
}
