part of '../sign_in_page.dart';

class _UpperHalf extends StatefulWidget {
  final SignInController _signInController;

  const _UpperHalf({Key? key, required SignInController signInController})
      : _signInController = signInController,
        super(key: key);

  @override
  State<_UpperHalf> createState() => _UpperHalfState();
}

class _UpperHalfState extends State<_UpperHalf> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _emailTEC = TextEditingController();
  final TextEditingController _passwordTEC = TextEditingController();
  final FocusNode _emailFocusNode = FocusNode();

  @override
  void dispose() {
    _emailTEC.dispose();
    _passwordTEC.dispose();
    _emailFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40)
          .copyWith(top: HomePage.padding, bottom: 30),
      child: Column(
        children: [
          const AppLogo(),
          const SizedBox(height: 30),
          Form(
            key: _formKey,
            child: Column(
              children: [
                CustomTextFormField(
                  controller: _emailTEC,
                  label: 'Email',
                  keyboardType: TextInputType.emailAddress,
                  validator: widget._signInController.validateEmail,
                  textInputAction: TextInputAction.next,
                  focusNode: _emailFocusNode,
                ),
                const SizedBox(height: 20),
                CustomTextFormField(
                  controller: _passwordTEC,
                  label: 'Password',
                  keyboardType: TextInputType.visiblePassword,
                  validator: widget._signInController.validatePassword,
                  textInputAction: TextInputAction.done,
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                      onPressed: _onForgotPasswordClicked,
                      child: const Text('Forgot your password?'),
                    ),
                    ElevatedButton(
                      onPressed: _onSignInClicked,
                      child: const Text('Sign in'),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onForgotPasswordClicked() {
    if (_emailTEC.text.isEmpty) {
      CustomSnackBar(
        type: SnackBarType.info,
        message: 'Fill an email to recover your password.',
      ).show(context);
      _emailFocusNode.requestFocus();
      return;
    }

    widget._signInController.recoverPassword(email: _emailTEC.text);
  }

  void _onSignInClicked() {
    final isFormValid = _formKey.currentState?.validate() ?? false;

    if (!isFormValid) return;

    widget._signInController.signInWithEmailAndPassword(
      email: _emailTEC.text,
      password: _passwordTEC.text,
    );
  }
}
