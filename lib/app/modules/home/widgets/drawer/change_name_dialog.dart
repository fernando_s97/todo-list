import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:todo/app/core/helpers/custom_navigator.dart';
import 'package:todo/app/core/widgets/custom_text_form_field.dart';
import 'package:todo/app/modules/home/widgets/drawer/home_drawer_controller.dart';

class ChangeNameDialog extends StatefulWidget {
  late final TextEditingController _newNameTEC;
  final HomeDrawerController _homeDrawerController;

  ChangeNameDialog({
    Key? key,
    required HomeDrawerController homeDrawerController,
  })  : _homeDrawerController = homeDrawerController,
        _newNameTEC = TextEditingController(
          text: homeDrawerController.currentUser?.displayName ?? '',
        ),
        super(key: key);

  @override
  State<ChangeNameDialog> createState() => _ChangeNameDialogState();
}

class _ChangeNameDialogState extends State<ChangeNameDialog> {
  final ValueNotifier<String?> _invalidNewNameErrorText = ValueNotifier(null);

  @override
  void initState() {
    super.initState();

    widget._homeDrawerController.addListener(_onControllerStateChanged);
  }

  void _onControllerStateChanged() {
    final shouldShowLoadingOverlay =
        widget._homeDrawerController.isChangingUserName;
    if (shouldShowLoadingOverlay) {
      Loader.show(context);
    } else {
      Loader.hide();
    }

    final changeUserNameResult =
        widget._homeDrawerController.changeUserNameResult;
    if (changeUserNameResult != null && changeUserNameResult) {
      CustomNavigator.it.pop();
    }
  }

  @override
  void dispose() {
    widget._homeDrawerController.removeListener(_onControllerStateChanged);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Change name'),
      content: ValueListenableBuilder<String?>(
        valueListenable: _invalidNewNameErrorText,
        builder: (context, invalidNewNameErrorText, child) {
          return CustomTextFormField(
            label: 'New name',
            controller: widget._newNameTEC,
            onChanged: _onNewNameChanged,
            textInputAction: TextInputAction.done,
            errorText: invalidNewNameErrorText,
          );
        },
      ),
      actions: [
        TextButton(
          onPressed: _onCancelClicked,
          child: const Text(
            'Cancel',
            style: TextStyle(color: Colors.red),
          ),
        ),
        ValueListenableBuilder<String?>(
          valueListenable: _invalidNewNameErrorText,
          builder: (context, invalidNewNameMessage, child) {
            final isNewNameValid = invalidNewNameMessage == null;
            return TextButton(
              onPressed: isNewNameValid ? _onChangeClicked : null,
              child: child!,
            );
          },
          child: const Text('Change'),
        ),
      ],
    );
  }

  void _onNewNameChanged(String? newName) {
    _invalidNewNameErrorText.value = _validateNewName(newName);
  }

  String? _validateNewName(String? newName) {
    String? errorText;

    if (newName == null || newName.isEmpty) {
      errorText = 'A name is required';
    }

    return errorText;
  }

  void _onCancelClicked() => CustomNavigator.it.pop();

  void _onChangeClicked() {
    widget._homeDrawerController.changeUserName(widget._newNameTEC.text);
  }
}
