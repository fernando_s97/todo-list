import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/models/task_filter.dart';
import 'package:todo/app/modules/home/home_controller.dart';
import 'package:todo/app/modules/home/home_page.dart';
import 'package:todo/app/modules/home/widgets/filters/filter_card.dart';

class HomeFilters extends StatelessWidget {
  const HomeFilters({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeController = context.read<HomeController>();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: HomePage.padding,
          ),
          child: Text('FILTERS', style: context.titleStyle),
        ),
        const SizedBox(height: 10),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              const SizedBox(width: HomePage.padding),
              FilterCard(
                filter: TaskFilter.today,
                homeController: homeController,
              ),
              const SizedBox(width: 10),
              FilterCard(
                filter: TaskFilter.tomorrow,
                homeController: homeController,
              ),
              const SizedBox(width: 10),
              FilterCard(
                filter: TaskFilter.week,
                homeController: homeController,
              ),
              const SizedBox(width: HomePage.padding),
            ],
          ),
        ),
      ],
    );
  }
}
