import 'package:provider/provider.dart';
import 'package:todo/app/core/modules/base_module.dart';
import 'package:todo/app/modules/auth/sign_in/sign_in_controller.dart';
import 'package:todo/app/modules/auth/sign_in/sign_in_page.dart';
import 'package:todo/app/modules/auth/sign_up/sign_up_controller.dart';
import 'package:todo/app/modules/auth/sign_up/sign_up_page.dart';

class AuthModule extends BaseModule {
  AuthModule()
      : super(
          bindings: [
            ChangeNotifierProvider(
              create: (context) {
                return SignInController(
                  userService: context.read(),
                  validations: context.read(),
                );
              },
            ),
            ChangeNotifierProvider(
              create: (context) {
                return SignUpController(
                  userService: context.read(),
                  validations: context.read(),
                );
              },
            ),
          ],
          routes: {
            SignInPage.routeName: (context) {
              return SignInPage(
                signInController: context.read(),
              );
            },
            SignUpPage.routeName: (context) {
              return SignUpPage(
                signUpController: context.read(),
              );
            },
          },
        );
}
