import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {
  String toIso8601StringWithTimeZone() {
    String isoTimestampWithoutTimeZone = toIso8601String();
    if (isUtc) {
      isoTimestampWithoutTimeZone =
          isoTimestampWithoutTimeZone.replaceAll('Z', '');
    }

    final formattedTimeZone = _formatTimeZone(timeZoneOffset);

    return '$isoTimestampWithoutTimeZone$formattedTimeZone';
  }

  String _formatTimeZone(Duration timeZoneOffset) {
    final signal = timeZoneOffset.isNegative ? '-' : '+';

    final positiveTimeCone = timeZoneOffset.abs();
    final twoDigitHours = _formatWithTwoDigits(positiveTimeCone.inHours);
    final twoDigitMinutes =
        _formatWithTwoDigits(positiveTimeCone.inMinutes.remainder(60));

    return '$signal$twoDigitHours:$twoDigitMinutes';
  }

  String _formatWithTwoDigits(int number) => number.toString().padLeft(2, '0');

  String get formatWithDateOnly => DateFormat('dd/MM/yyyy').format(this);

  DateTime getMostRecentMonday() {
    final mondayOffset = weekday - 1;

    return subtract(Duration(days: mondayOffset));
  }
}
