import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/task_filter_extension.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/models/task_filter.dart';
import 'package:todo/app/modules/home/home_controller.dart';

class FilterCard extends StatelessWidget {
  final TaskFilter _filter;
  final HomeController _homeController;

  const FilterCard({
    Key? key,
    required TaskFilter filter,
    required HomeController homeController,
  })  : _filter = filter,
        _homeController = homeController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(30);
    return InkWell(
      borderRadius: borderRadius,
      onTap: _onFilterClicked,
      child: Selector<HomeController, TaskFilter>(
        selector: (context, controller) => controller.selectedFilter,
        builder: (context, selectedFilter, child) {
          final isSelected = _filter == selectedFilter;

          final cardColor = isSelected ? context.primaryColor : Colors.white;
          final tasksCountColor = isSelected ? Colors.white : Colors.black45;
          final filterColor = isSelected ? Colors.white : Colors.black45;
          final progressBackgroundColor =
              isSelected ? context.primaryColorLight : Colors.grey.shade400;
          final progressColor =
              isSelected ? Colors.white : context.primaryColor;

          return Container(
            width: 170,
            height: 120,
            decoration: BoxDecoration(
              color: cardColor,
              border: Border.all(
                color: Colors.grey.withOpacity(0.8),
              ),
              borderRadius: borderRadius,
            ),
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Selector<HomeController, int>(
                    selector: (context, controller) {
                      return controller.getTasksCountByFilter(_filter);
                    },
                    builder: (context, tasksCount, child) {
                      return Text(
                        _buildTasksCountDescription(tasksCount),
                        style: context.titleStyle.copyWith(
                          fontSize: 12.5,
                          color: tasksCountColor,
                        ),
                      );
                    },
                  ),
                  const Spacer(),
                  Text(
                    _filter.description.toUpperCase(),
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: filterColor,
                    ),
                  ),
                  Selector<HomeController, double>(
                    selector: (context, controller) {
                      return controller.getTasksProgressByFilter(_filter);
                    },
                    builder: (context, tasksProgress, child) {
                      return TweenAnimationBuilder<double>(
                        tween: Tween(begin: 0, end: tasksProgress),
                        duration: const Duration(milliseconds: 750),
                        builder: (context, animationProgress, child) {
                          return LinearProgressIndicator(
                            value: animationProgress,
                            backgroundColor: progressBackgroundColor,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(progressColor),
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _onFilterClicked() {
    _homeController.selectedFilter = _filter;
  }

  String _buildTasksCountDescription(int tasksCount) {
    if (tasksCount == 0) {
      return 'NO TASKS';
    } else if (tasksCount == 1) {
      return '$tasksCount TASK';
    } else if (tasksCount >= 2) {
      return '$tasksCount TASKS';
    } else {
      throw Exception('Negative tasks count should never happen');
    }
  }
}
