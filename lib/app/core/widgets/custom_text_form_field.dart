import 'package:flutter/material.dart';
import 'package:todo/app/core/ui/custom_icons.dart';

class CustomTextFormField extends StatelessWidget {
  static const double borderRadius = 30;

  final String _label;
  final TextInputType _keyboardType;
  final ValueNotifier<bool> _obscureText;
  final TextEditingController? _controller;
  final FormFieldValidator<String>? _validator;
  final TextInputAction? _textInputAction;
  final FocusNode? _focusNode;
  final ValueChanged<String>? _onChanged;
  final String? _errorText;

  CustomTextFormField({
    Key? key,
    required String label,
    TextInputType keyboardType = TextInputType.text,
    TextEditingController? controller,
    FormFieldValidator<String>? validator,
    TextInputAction? textInputAction,
    FocusNode? focusNode,
    ValueChanged<String>? onChanged,
    String? errorText,
  })  : _label = label,
        _keyboardType = keyboardType,
        _obscureText = ValueNotifier(
          keyboardType == TextInputType.visiblePassword,
        ),
        _controller = controller,
        _validator = validator,
        _textInputAction = textInputAction,
        _focusNode = focusNode,
        _onChanged = onChanged,
        _errorText = errorText,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final baseBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(borderRadius),
    );

    return ValueListenableBuilder<bool>(
      valueListenable: _obscureText,
      builder: (context, isTextObscured, child) {
        Widget? suffixIcon;
        if (_keyboardType == TextInputType.visiblePassword) {
          if (isTextObscured) {
            suffixIcon = IconButton(
              onPressed: () => _obscureText.value = false,
              icon: const Icon(CustomIcons.visibilityOff),
            );
          } else {
            suffixIcon = IconButton(
              onPressed: () => _obscureText.value = true,
              icon: const Icon(CustomIcons.visibility),
            );
          }
        }

        return TextFormField(
          decoration: InputDecoration(
            isDense: true,
            labelText: _label,
            border: baseBorder,
            errorBorder: baseBorder.copyWith(
              borderSide: const BorderSide(color: Colors.red),
            ),
            suffixIcon: suffixIcon,
            errorText: _errorText,
          ),
          keyboardType: _keyboardType,
          textInputAction: _textInputAction,
          obscureText: isTextObscured,
          controller: _controller,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: _validator,
          focusNode: _focusNode,
          onChanged: _onChanged,
        );
      },
    );
  }
}
