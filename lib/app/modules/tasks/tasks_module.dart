import 'package:provider/provider.dart';
import 'package:todo/app/core/modules/base_module.dart';
import 'package:todo/app/modules/tasks/create_task_controller.dart';
import 'package:todo/app/modules/tasks/create_task_page.dart';

class TasksModule extends BaseModule {
  TasksModule()
      : super(
          bindings: [
            ChangeNotifierProvider(
              create: (context) => CreateTaskController(
                tasksService: context.read(),
                validations: context.read(),
              ),
            ),
          ],
          routes: {
            CreateTaskPage.routeName: (context) {
              return CreateTaskPage(createTaskController: context.read());
            },
          },
        );
}
