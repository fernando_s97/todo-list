import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/helpers/custom_navigator.dart';
import 'package:todo/app/core/ui/custom_icons.dart';
import 'package:todo/app/modules/home/home_controller.dart';
import 'package:todo/app/modules/home/widgets/drawer/home_drawer.dart';
import 'package:todo/app/modules/home/widgets/filters/home_filters.dart';
import 'package:todo/app/modules/home/widgets/filters/home_week_filter.dart';
import 'package:todo/app/modules/home/widgets/header/home_header.dart';
import 'package:todo/app/modules/home/widgets/tasks/home_tasks.dart';
import 'package:todo/app/modules/tasks/create_task_page.dart';
import 'package:todo/app/modules/tasks/tasks_module.dart';

class HomePage extends StatelessWidget {
  static const String routeName = '/home';
  static const double padding = 16;

  final HomeController _homeController;

  HomePage({Key? key, required HomeController homeController})
      : _homeController = homeController,
        super(key: key) {
    _homeController.reloadAllTasks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Page'),
        actions: [
          PopupMenuButton(
            icon: const Icon(CustomIcons.filter),
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: ChangeNotifierProvider.value(
                    value: _homeController,
                    child: Selector<HomeController, bool>(
                      selector: (context, controller) {
                        return controller.shouldShowOnlyUndoneTasks;
                      },
                      builder: (context, showOnlyUndoneTasks, child) {
                        return CheckboxListTile(
                          title: const Text('Show undone tasks only'),
                          value: showOnlyUndoneTasks,
                          onChanged: _onShowUndoneTasksOnlyClicked,
                        );
                      },
                    ),
                  ),
                ),
              ];
            },
          ),
        ],
      ),
      drawer: HomeDrawer(
        homeDrawerController: context.read(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onAddClicked,
        child: const Icon(CustomIcons.add),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: HomePage.padding,
                    ),
                    child: HomeHeader(),
                  ),
                  const SizedBox(height: 20),
                  const HomeFilters(),
                  const SizedBox(height: 20),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: HomePage.padding,
                      ),
                      child: Column(
                        children: [
                          HomeWeekFilter(homeController: context.read()),
                          const SizedBox(height: 20),
                          Expanded(
                            child: HomeTasks(homeController: context.read()),
                          ),
                          const SizedBox(height: HomePage.padding),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onShowUndoneTasksOnlyClicked(bool? isSelected) {
    _homeController.toggleShowOnlyUndoneTasks();
  }

  Future<void> _onAddClicked() async {
    await CustomNavigator.it.push(
      PageRouteBuilder(
        transitionDuration: const Duration(milliseconds: 400),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          final curvedAnimation = CurvedAnimation(
            parent: animation,
            curve: Curves.easeInQuad,
          );

          return ScaleTransition(
            scale: curvedAnimation,
            alignment: Alignment.bottomRight,
            child: child,
          );
        },
        pageBuilder: (context, animation, secondaryAnimation) {
          final builder = TasksModule().routes[CreateTaskPage.routeName]!;
          return builder(context);
        },
      ),
    );

    _homeController.reloadAllTasks();
  }
}
