import 'dart:developer' as dev;

import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:todo/app/core/exceptions/auth_exception.dart';
import 'package:todo/app/core/helpers/validations.dart';
import 'package:todo/app/services/user/user_service.dart';

class SignInController extends ChangeNotifier {
  final UserService _userService;
  final Validations _validations;

  bool get isSigningIn => _isSigningIn;
  bool _isSigningIn = false;
  Either<String, Unit>? _signInResult;

  bool _isRecoveringPassword = false;
  Either<String, Unit>? _recoverPasswordResult;

  SignInController({
    required UserService userService,
    required Validations validations,
  })  : _userService = userService,
        _validations = validations;

  Either<String, Unit>? get signInResult => _signInResult;

  bool get isRecoveringPassword => _isRecoveringPassword;

  Either<String, Unit>? get recoverPasswordResult => _recoverPasswordResult;

  Future<void> signInWithEmailAndPassword({
    required String email,
    required String password,
  }) async {
    _isSigningIn = true;
    _signInResult = null;
    notifyListeners();

    try {
      final user = await _userService.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      _signInResult = user == null
          ? const Left('Invalid email and/or password.')
          : const Right(unit);
    } on AuthException catch (e) {
      _signInResult = Left(e.message);
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      _signInResult = const Left('Unexpected error while trying to sign in.');
    } finally {
      _isSigningIn = false;
      notifyListeners();

      _signInResult = null;
      notifyListeners();
    }
  }

  Future<void> recoverPassword({required String email}) async {
    _isRecoveringPassword = true;
    _recoverPasswordResult = null;
    notifyListeners();

    try {
      await _userService.recoverPassword(email: email);

      _recoverPasswordResult = const Right(unit);
    } on AuthException catch (e) {
      _recoverPasswordResult = Left(e.message);
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      _recoverPasswordResult =
          const Left('Unexpected error while trying to recover the password.');
    } finally {
      _isRecoveringPassword = false;
      notifyListeners();

      _recoverPasswordResult = null;
      notifyListeners();
    }
  }

  Future<void> signInWithGoogle() async {
    _isSigningIn = true;
    _signInResult = null;
    notifyListeners();

    try {
      final user = await _userService.signInWithGoogle();

      _signInResult = user == null ? null : const Right(unit);
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      if (e is AuthException) {
        _signInResult = Left(e.message);
      } else {
        _signInResult = const Left(
          'Unexpected error while trying to sign in with Google.',
        );
      }

      await _userService.signOut();
    } finally {
      _isSigningIn = false;
      notifyListeners();

      _signInResult = null;
      notifyListeners();
    }
  }

  String? validateEmail(String? email) {
    return _validations.validateEmail(email);
  }

  String? validatePassword(String? password) {
    return _validations.validatePassword(password);
  }
}
