import 'package:flutter/material.dart';
import 'package:todo/app/core/extensions/date_time_extension.dart';
import 'package:todo/app/core/ui/custom_icons.dart';
import 'package:todo/app/models/task_model.dart';

class TaskWidget extends StatelessWidget {
  final TaskModel _task;
  final ValueChanged<bool?> _onDoneChanged;
  final VoidCallback _onDeleteClicked;

  const TaskWidget({
    Key? key,
    required TaskModel task,
    required ValueChanged<bool?> onDoneChanged,
    required VoidCallback onDeleteClicked,
  })  : _task = task,
        _onDoneChanged = onDoneChanged,
        _onDeleteClicked = onDeleteClicked,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(
      decoration: _task.isDone ? TextDecoration.lineThrough : null,
    );

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: Colors.grey.shade300),
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.all(8),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
          side: const BorderSide(),
        ),
        leading: Checkbox(
          value: _task.isDone,
          onChanged: _onDoneChanged,
        ),
        title: Text(_task.description, style: textStyle),
        subtitle: Text(_task.dueDate.formatWithDateOnly, style: textStyle),
        trailing: IconButton(
          onPressed: _onDeleteClicked,
          icon: Icon(CustomIcons.trash),
        ),
      ),
    );
  }
}
