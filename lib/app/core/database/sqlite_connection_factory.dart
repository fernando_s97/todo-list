import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:synchronized/synchronized.dart';
import 'package:todo/app/core/database/sqlite_migration_factory.dart';

class SqliteConnectionFactory {
  static const int _version = 1;
  static const String _databaseName = 'todo_list';

  static late final SqliteConnectionFactory instance =
      SqliteConnectionFactory._();

  final Lock _lock = Lock();

  Database? _database;

  SqliteConnectionFactory._();

  Future<Database> openConnection() async {
    await _lock.synchronized(() async {
      if (_database != null) return;

      final deviceDatabasePath = await getDatabasesPath();
      final fullDatabasePath = join(deviceDatabasePath, _databaseName);

      _database = await openDatabase(
        fullDatabasePath,
        version: _version,
        onConfigure: _onConfigure,
        onCreate: _onCreate,
        onUpgrade: _onUpgrade,
        onDowngrade: _onDowngrade,
      );
    });

    return _database!;
  }

  Future<void> _onConfigure(Database db) async {
    await db.execute('PRAGMA fereign_keys = ON');
  }

  Future<void> _onCreate(Database db, int version) async {
    final batch = db.batch();

    final migrations = SqliteMigrationFactory().getCreateMigrations();
    for (final migration in migrations) {
      await migration.onCreate(batch);
    }

    batch.commit();
  }

  Future<void> _onUpgrade(Database db, int oldVersion, int version) async {
    final batch = db.batch();

    final migrations =
        SqliteMigrationFactory().getUpgradeMigrations(oldVersion);
    for (final migration in migrations) {
      await migration.onUpgrade(batch);
    }

    batch.commit();
  }

  Future<void> _onDowngrade(Database db, int oldVersion, int version) async {}

  void closeConnection() {
    _database?.close();
    _database = null;
  }
}
