part of '../sign_in_page.dart';

class _BottomHalf extends StatelessWidget {
  final SignInController _signInController;

  const _BottomHalf({Key? key, required SignInController signInController})
      : _signInController = signInController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: const Color(0xfff0f3f7),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40)
            .copyWith(top: 30, bottom: HomePage.padding),
        child: Column(
          children: [
            SignInButton(
              Buttons.Google,
              padding: const EdgeInsets.all(5),
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              onPressed: _onSignInWithGoogleClicked,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Doesn't have an account?"),
                TextButton(
                  onPressed: () => _onSignUpClicked(context),
                  child: const Text('Sign up'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _onSignInWithGoogleClicked() {
    _signInController.signInWithGoogle();
  }

  void _onSignUpClicked(BuildContext context) {
    CustomNavigator.it.pushNamed(SignUpPage.routeName);
  }
}
