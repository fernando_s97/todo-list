import 'package:flutter/material.dart';
import 'package:validatorless/validatorless.dart';

class Validations {
  const Validations();

  String? validateEmail(String? email) {
    return Validatorless.multiple([
      Validatorless.required('Email is required'),
      Validatorless.email('Invalid email'),
    ])(email);
  }

  int get _passwordMinLength => 6;

  String? validatePassword(String? password) {
    return Validatorless.multiple([
      Validatorless.required('Password is required'),
      Validatorless.min(
        _passwordMinLength,
        'The password must be at least $_passwordMinLength characters long',
      ),
    ])(password);
  }

  String? validateConfirmPassword({
    required String? password,
    required String? confirmPassword,
  }) {
    return Validatorless.multiple([
      Validatorless.required('Password is required'),
      _compare(password, "Passwords don't match"),
    ])(confirmPassword);
  }

  FormFieldValidator<String> _compare(
    String? valueToBeMatched,
    String message,
  ) {
    return (value) {
      if (value == null || value != valueToBeMatched) {
        return message;
      }

      return null;
    };
  }

  String? validateTaskDescription(String? description) {
    return Validatorless.required('Required')(description);
  }
}
