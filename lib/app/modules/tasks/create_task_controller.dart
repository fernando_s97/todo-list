import 'package:flutter/material.dart';
import 'package:todo/app/core/helpers/validations.dart';
import 'package:todo/app/services/tasks/tasks_service.dart';

class CreateTaskController extends ChangeNotifier {
  final TasksService _tasksService;
  final Validations _validations;

  String description = '';
  DateTime _dueDate = DateTime.now();
  bool _isCreatingTask = false;
  bool? _createTaskResult;

  CreateTaskController({
    required TasksService tasksService,
    required Validations validations,
  })  : _tasksService = tasksService,
        _validations = validations;

  DateTime get dueDate => _dueDate;

  set dueDate(DateTime dateTime) {
    _dueDate = dateTime;
    notifyListeners();
  }

  bool get isCreatingTask => _isCreatingTask;

  bool? get createTaskResult => _createTaskResult;

  Future<void> create() async {
    _isCreatingTask = true;
    notifyListeners();

    try {
      await _tasksService.create(description: description, dueDate: dueDate);
    } finally {
      _isCreatingTask = false;
      _createTaskResult = true;
      notifyListeners();

      _createTaskResult = null;
      notifyListeners();
    }
  }

  String? validateDescription(String? description) {
    return _validations.validateTaskDescription(description);
  }
}
