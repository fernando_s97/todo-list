import 'package:todo/app/models/filtered_tasks_result_model.dart';
import 'package:todo/app/models/task_model.dart';

abstract class TasksService {
  Future<void> create({required String description, required DateTime dueDate});

  Future<FilteredTasksResultModel> findToday();

  Future<FilteredTasksResultModel> findTomorrow();

  Future<FilteredTasksResultModel> findWeek();

  Future<void> changeDoneState(TaskModel task);

  Future<void> deleteTask({required int id});

  Future<void> clearAll();
}
