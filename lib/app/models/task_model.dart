import 'package:flutter/material.dart';
import 'package:todo/app/core/extensions/date_time_extension.dart';
import 'package:todo/app/core/helpers/utils.dart';

@immutable
class TaskModel {
  final int? id;
  final String description;
  final DateTime dueDate;
  final bool isDone;

  const TaskModel({
    this.id,
    required this.description,
    required this.dueDate,
    required this.isDone,
  });

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other is TaskModel &&
            runtimeType == other.runtimeType &&
            Utils.equality.equals(id, other.id) &&
            Utils.equality.equals(description, other.description) &&
            Utils.equality.equals(dueDate, other.dueDate) &&
            Utils.equality.equals(isDone, other.isDone));
  }

  @override
  int get hashCode =>
      Utils.equality.hash(id) ^
      Utils.equality.hash(description) ^
      Utils.equality.hash(dueDate) ^
      Utils.equality.hash(isDone);

  @override
  String toString() {
    return 'TaskModel(id: $id, description: $description, dueDate: $dueDate, '
        'isDone: $isDone)';
  }

  TaskModel copyWith({
    int? id,
    String? description,
    DateTime? dueDate,
    bool? isDone,
  }) {
    return TaskModel(
      id: id ?? this.id,
      description: description ?? this.description,
      dueDate: dueDate ?? this.dueDate,
      isDone: isDone ?? this.isDone,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'description': description,
      'dueDate': dueDate.toIso8601StringWithTimeZone(),
      'isDone': isDone,
    };
  }

  factory TaskModel.fromJson(Map<String, dynamic> map) {
    return TaskModel(
      id: map['id'] as int?,
      description: map['description'] as String,
      dueDate: DateTime.parse(map['dueDate'] as String),
      isDone: map['isDone'] as bool,
    );
  }
}
