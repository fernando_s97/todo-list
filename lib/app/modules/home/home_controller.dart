import 'package:flutter/material.dart';
import 'package:todo/app/models/task_filter.dart';
import 'package:todo/app/models/task_model.dart';
import 'package:todo/app/services/tasks/tasks_service.dart';

class HomeController extends ChangeNotifier {
  final TasksService _tasksService;

  TaskFilter _selectedFilter = TaskFilter.today;
  DateTime? _selectedDateForWeekFilter;
  bool _shouldShowOnlyUndoneTasks = false;
  List<TaskModel> _tasksToday = [];
  List<TaskModel> _tasksTomorrow = [];
  List<TaskModel> _tasksWeek = [];

  HomeController({required TasksService tasksService})
      : _tasksService = tasksService;

  TaskFilter get selectedFilter => _selectedFilter;

  set selectedFilter(TaskFilter selectedFilter) {
    _selectedFilter = selectedFilter;
    _selectedDateForWeekFilter =
        selectedFilter == TaskFilter.week ? DateTime.now() : null;
    notifyListeners();

    reloadAllTasks();
  }

  int getTasksCountByFilter(TaskFilter filter) {
    final tasks = _getTasksByFilter(filter);
    final undoneTasks = tasks.where((task) => !task.isDone);
    return undoneTasks.length;
  }

  double getTasksProgressByFilter(TaskFilter filter) {
    final tasks = _getTasksByFilter(filter);
    final finishedTasksCount = tasks.where((task) => task.isDone).length;

    return tasks.isNotEmpty ? (finishedTasksCount / tasks.length) : 1;
  }

  DateTime? get selectedDateForWeekFilter => _selectedDateForWeekFilter;

  set selectedDateForWeekFilter(DateTime? date) {
    _selectedDateForWeekFilter = date;
    notifyListeners();
  }

  List<TaskModel> get tasksForCurrentFilter {
    List<TaskModel> tasks = _getTasksByFilter(_selectedFilter);

    if (_selectedFilter == TaskFilter.week) {
      tasks = tasks.where((task) {
        return task.dueDate.day == _selectedDateForWeekFilter!.day;
      }).toList();
    }

    if (_shouldShowOnlyUndoneTasks) {
      tasks = tasks.where((task) => !task.isDone).toList();
    }

    return tasks;
  }

  List<TaskModel> _getTasksByFilter(TaskFilter filter) {
    final List<TaskModel> tasks;

    switch (filter) {
      case TaskFilter.today:
        tasks = _tasksToday;
        break;
      case TaskFilter.tomorrow:
        tasks = _tasksTomorrow;
        break;
      case TaskFilter.week:
        tasks = _tasksWeek;
        break;
    }
    return tasks;
  }

  Future<void> toggleTaskDoneState(TaskModel task) async {
    final updatedTask = task.copyWith(isDone: !task.isDone);
    await _tasksService.changeDoneState(updatedTask);
    reloadAllTasks();
  }

  bool get shouldShowOnlyUndoneTasks => _shouldShowOnlyUndoneTasks;

  void toggleShowOnlyUndoneTasks() {
    _shouldShowOnlyUndoneTasks = !_shouldShowOnlyUndoneTasks;
    notifyListeners();
  }

  Future<void> deleteTask(TaskModel task) async {
    await _tasksService.deleteTask(id: task.id!);
    reloadAllTasks();
  }

  Future<void> reloadAllTasks() async {
    final results = await Future.wait([
      _tasksService.findToday(),
      _tasksService.findTomorrow(),
      _tasksService.findWeek(),
    ]);

    _tasksToday = results[0].tasks;
    _tasksTomorrow = results[1].tasks;
    _tasksWeek = results[2].tasks;
    notifyListeners();
  }
}
