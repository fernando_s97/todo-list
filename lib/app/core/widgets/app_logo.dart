import 'package:flutter/material.dart';

class AppLogo extends StatelessWidget {
  final bool _withTitle;

  const AppLogo({Key? key, bool withTitle = true})
      : _withTitle = withTitle,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final image = Image.asset('assets/images/logo.png');

    if (!_withTitle) return image;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        image,
        Text(
          'Todo List',
          style: Theme.of(context).textTheme.headline6,
        ),
      ],
    );
  }
}
