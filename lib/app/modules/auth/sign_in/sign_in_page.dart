import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:todo/app/core/helpers/custom_navigator.dart';
import 'package:todo/app/core/helpers/custom_snack_bar.dart';
import 'package:todo/app/core/widgets/app_logo.dart';
import 'package:todo/app/core/widgets/custom_text_form_field.dart';
import 'package:todo/app/modules/auth/sign_in/sign_in_controller.dart';
import 'package:todo/app/modules/auth/sign_up/sign_up_page.dart';
import 'package:todo/app/modules/home/home_page.dart';

part 'widgets/bottom_half.dart';
part 'widgets/upper_half.dart';

class SignInPage extends StatefulWidget {
  static const String routeName = '/signIn';

  final SignInController _signInController;

  const SignInPage({Key? key, required SignInController signInController})
      : _signInController = signInController,
        super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  void initState() {
    super.initState();

    widget._signInController.addListener(_onControllerStateChanged);
  }

  void _onControllerStateChanged() {
    final hasSomethingInProgress = widget._signInController.isSigningIn ||
        widget._signInController.isRecoveringPassword;
    if (hasSomethingInProgress) {
      Loader.show(context);
    } else {
      Loader.hide();
    }

    if (!widget._signInController.isSigningIn) {
      widget._signInController.signInResult?.fold(
        (errorMessage) => CustomSnackBar(
          type: SnackBarType.error,
          message: errorMessage,
        ).show(context),
        (success) {},
      );
    }

    if (!widget._signInController.isRecoveringPassword) {
      widget._signInController.recoverPasswordResult?.fold(
        (errorMessage) => CustomSnackBar(
          type: SnackBarType.error,
          message: errorMessage,
        ).show(context),
        (success) => CustomSnackBar(
          type: SnackBarType.success,
          message: 'The instructions to recover your password were sent to '
              'your email.',
        ).show(context),
      );
    }
  }

  @override
  void dispose() {
    widget._signInController.removeListener(_onControllerStateChanged);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                children: [
                  _UpperHalf(signInController: widget._signInController),
                  Divider(
                    height: 2,
                    thickness: 2,
                    color: Colors.grey.withAlpha(50),
                  ),
                  Expanded(
                    child: _BottomHalf(
                      signInController: widget._signInController,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
