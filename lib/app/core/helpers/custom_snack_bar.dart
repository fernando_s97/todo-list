import 'package:flutter/material.dart';

enum SnackBarType { success, error, info }

class CustomSnackBar {
  final SnackBarType _type;
  final String _message;

  CustomSnackBar({required SnackBarType type, required String message})
      : _type = type,
        _message = message;

  void show(BuildContext context) {
    final Color backgroundColor;
    switch (_type) {
      case SnackBarType.success:
        backgroundColor = Colors.green;
        break;
      case SnackBarType.error:
        backgroundColor = Theme.of(context).colorScheme.error;
        break;
      case SnackBarType.info:
        backgroundColor = Colors.blue;
        break;
    }

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(_message),
        backgroundColor: backgroundColor,
      ),
    );
  }
}
