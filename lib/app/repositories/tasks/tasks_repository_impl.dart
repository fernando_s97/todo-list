import 'package:todo/app/core/database/sqlite_connection_factory.dart';
import 'package:todo/app/core/extensions/date_time_extension.dart';
import 'package:todo/app/mappers/task_mapper.dart';
import 'package:todo/app/models/task_model.dart';
import 'package:todo/app/repositories/tasks/tasks_repository.dart';

class TasksRepositoryImpl implements TasksRepository {
  final SqliteConnectionFactory _sqliteConnectionFactory;
  final TaskMapper _taskMapper;

  TasksRepositoryImpl({
    required SqliteConnectionFactory sqliteConnectionFactory,
    required TaskMapper taskMapper,
  })  : _sqliteConnectionFactory = sqliteConnectionFactory,
        _taskMapper = taskMapper;

  @override
  Future<void> create({required TaskModel task}) async {
    final connection = await _sqliteConnectionFactory.openConnection();

    final taskDatabase = _taskMapper.toDatabase(task);

    await connection.insert('Todo', taskDatabase);
  }

  @override
  Future<List<TaskModel>> findByPeriod({
    required DateTime from,
    required DateTime upTo,
  }) async {
    final start = DateTime(from.year, from.month, from.day);
    final finish =
        DateTime(upTo.year, upTo.month, upTo.day, 23, 59, 59, 999, 999);

    final connection = await _sqliteConnectionFactory.openConnection();

    final result = await connection.query(
      'Todo',
      where: 'dueDate between ? and ?',
      whereArgs: [
        start.toIso8601StringWithTimeZone(),
        finish.toIso8601StringWithTimeZone(),
      ],
      orderBy: 'dueDate',
    );

    return result.map(_taskMapper.fromDatabase).toList();
  }

  @override
  Future<void> changeDoneState(TaskModel task) async {
    final isDoneDatabase = _taskMapper.toDatabase(task)['isDone'] as int;

    final connection = await _sqliteConnectionFactory.openConnection();

    await connection.update(
      'Todo',
      {'isDone': isDoneDatabase},
      where: 'id = ?',
      whereArgs: [task.id],
    );
  }

  @override
  Future<void> deleteTask({required int id}) async {
    final connection = await _sqliteConnectionFactory.openConnection();

    await connection.delete('Todo', where: 'id = ?', whereArgs: [id]);
  }

  @override
  Future<void> clearAll() async {
    final connection = await _sqliteConnectionFactory.openConnection();

    await connection.delete('Todo');
  }
}
