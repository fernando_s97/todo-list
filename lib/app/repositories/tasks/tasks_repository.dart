import 'package:todo/app/models/task_model.dart';

abstract class TasksRepository {
  Future<void> create({required TaskModel task});

  Future<List<TaskModel>> findByPeriod({
    required DateTime from,
    required DateTime upTo,
  });

  Future<void> changeDoneState(TaskModel task);

  Future<void> deleteTask({required int id});

  Future<void> clearAll();
}
