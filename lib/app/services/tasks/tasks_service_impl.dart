import 'package:todo/app/models/filtered_tasks_result_model.dart';
import 'package:todo/app/models/task_model.dart';
import 'package:todo/app/repositories/tasks/tasks_repository.dart';
import 'package:todo/app/services/tasks/tasks_service.dart';

class TasksServiceImpl implements TasksService {
  final TasksRepository _tasksRepository;

  TasksServiceImpl({required TasksRepository tasksRepository})
      : _tasksRepository = tasksRepository;

  @override
  Future<void> create({
    required String description,
    required DateTime dueDate,
  }) {
    final task = TaskModel(
      description: description,
      dueDate: dueDate,
      isDone: false,
    );

    return _tasksRepository.create(task: task);
  }

  @override
  Future<FilteredTasksResultModel> findToday() async {
    final today = DateTime.now();

    final tasks = await _tasksRepository.findByPeriod(from: today, upTo: today);

    return FilteredTasksResultModel(from: today, upTo: today, tasks: tasks);
  }

  @override
  Future<FilteredTasksResultModel> findTomorrow() async {
    final tomorrow = DateTime.now().add(const Duration(days: 1));

    final tasks =
        await _tasksRepository.findByPeriod(from: tomorrow, upTo: tomorrow);

    return FilteredTasksResultModel(from: tomorrow, upTo: tomorrow, tasks: tasks);
  }

  @override
  Future<FilteredTasksResultModel> findWeek() async {
    final today = DateTime.now();

    final offsetToMonday = today.weekday - 1;
    final monday = today.subtract(Duration(days: offsetToMonday));

    final sunday = monday.add(const Duration(days: 7));

    final tasks =
        await _tasksRepository.findByPeriod(from: monday, upTo: sunday);

    return FilteredTasksResultModel(from: monday, upTo: sunday, tasks: tasks);
  }

  @override
  Future<void> changeDoneState(TaskModel task) {
    return _tasksRepository.changeDoneState(task);
  }

  @override
  Future<void> deleteTask({required int id}) {
    return _tasksRepository.deleteTask(id: id);
  }

  @override
  Future<void> clearAll() {
    return _tasksRepository.clearAll();
  }
}
