import 'package:sqflite/sqflite.dart';
import 'package:todo/app/core/database/migrations/migration.dart';

class MigrationV1 implements Migration {
  @override
  Future<void> onCreate(Batch batch) async {
    batch.execute(
      '''
        CREATE TABLE Todo(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          description VARCHAR(500) NOT NULL,
          dueDate DATETIME NOT NULL,
          isDone INTEGER NOT NULL
        )
      ''',
    );
  }

  @override
  Future<void> onUpgrade(Batch batch) async {}
}
