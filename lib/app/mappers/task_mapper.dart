import 'package:todo/app/models/task_model.dart';

class TaskMapper {
  const TaskMapper();

  TaskModel fromDatabase(Map<String, dynamic> taskDb) {
    final mutableTaskDb = Map<String, dynamic>.from(taskDb);

    mutableTaskDb['isDone'] = mutableTaskDb['isDone'] as int == 1;

    return TaskModel.fromJson(mutableTaskDb);
  }

  Map<String, dynamic> toDatabase(TaskModel task) {
    final taskDatabase = task.toJson();

    taskDatabase['isDone'] = taskDatabase['isDone'] as bool ? 1 : 0;

    return taskDatabase;
  }
}
