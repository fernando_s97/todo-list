import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/app_widget.dart';
import 'package:todo/app/core/database/sqlite_connection_factory.dart';
import 'package:todo/app/core/helpers/validations.dart';
import 'package:todo/app/mappers/task_mapper.dart';
import 'package:todo/app/repositories/tasks/tasks_repository_impl.dart';
import 'package:todo/app/repositories/user/user_repository_impl.dart';
import 'package:todo/app/services/tasks/tasks_service.dart';
import 'package:todo/app/services/tasks/tasks_service_impl.dart';
import 'package:todo/app/services/user/user_service.dart';
import 'package:todo/app/services/user/user_service_impl.dart';

class AppModule extends StatelessWidget {
  const AppModule({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => const Validations()),
        Provider<UserService>(
          create: (context) {
            return UserServiceImpl(
              userRepository: UserRepositoryImpl(
                firebaseAuth: FirebaseAuth.instance,
                googleSignIn: GoogleSignIn(),
              ),
            );
          },
        ),
        Provider(
          create: (context) => SqliteConnectionFactory.instance,
          lazy: false,
        ),
        Provider<TasksService>(
          create: (context) => TasksServiceImpl(
            tasksRepository: TasksRepositoryImpl(
              sqliteConnectionFactory: context.read(),
              taskMapper: const TaskMapper(),
            ),
          ),
        ),
      ],
      child: Builder(
        builder: (context) {
          return AppWidget(
            userService: context.read(),
          );
        },
      ),
    );
  }
}
