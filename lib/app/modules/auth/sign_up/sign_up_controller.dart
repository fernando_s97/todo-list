import 'dart:developer' as dev;

import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:todo/app/core/exceptions/auth_exception.dart';
import 'package:todo/app/core/helpers/validations.dart';
import 'package:todo/app/services/user/user_service.dart';

class SignUpController extends ChangeNotifier {
  final UserService _userService;
  final Validations _validations;

  bool _isSigningUp = false;
  Either<String, Unit>? _signUpResult;

  SignUpController({
    required UserService userService,
    required Validations validations,
  })  : _userService = userService,
        _validations = validations;

  bool get isSigningUp => _isSigningUp;

  Either<String, Unit>? get signUpResult => _signUpResult;

  Future<void> signUp({
    required String email,
    required String password,
  }) async {
    _isSigningUp = true;
    _signUpResult = null;
    notifyListeners();

    try {
      final user = await _userService.signUp(email: email, password: password);

      _signUpResult = user == null
          ? const Left('Failed to sign up user.')
          : const Right(unit);
    } on AuthException catch (e) {
      _signUpResult = Left(e.message);
    } catch (e, s) {
      dev.log(e.toString(), stackTrace: s);

      _signUpResult =
          const Left('Unexpected error while trying to sign up user.');
    } finally {
      _isSigningUp = false;
      notifyListeners();

      _signUpResult = null;
      notifyListeners();
    }
  }

  String? validateEmail(String? email) {
    return _validations.validateEmail(email);
  }

  String? validatePassword(String? password) {
    return _validations.validatePassword(password);
  }

  String? validateConfirmPassword({
    required String password,
    required String? confirmPassword,
  }) {
    return _validations.validateConfirmPassword(
      password: password,
      confirmPassword: confirmPassword,
    );
  }
}
