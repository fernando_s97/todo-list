import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo/app/core/extensions/date_time_extension.dart';
import 'package:todo/app/core/ui/custom_icons.dart';
import 'package:todo/app/core/widgets/custom_text_form_field.dart';
import 'package:todo/app/modules/tasks/create_task_controller.dart';

class DueDateButton extends StatelessWidget {
  final CreateTaskController _createTaskController;

  const DueDateButton({
    Key? key,
    required CreateTaskController createTaskController,
  })  : _createTaskController = createTaskController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final borderRadius =
        BorderRadius.circular(CustomTextFormField.borderRadius);

    return InkWell(
      onTap: () => _onSetDueDateClicked(context),
      borderRadius: borderRadius,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
            side: const BorderSide(color: Colors.grey),
            borderRadius: borderRadius,
          ),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(CustomIcons.today, color: Colors.grey),
            const SizedBox(width: 5),
            const Text('Due on '),
            Selector<CreateTaskController, DateTime>(
              selector: (context, controller) => controller.dueDate,
              builder: (context, dueDate, child) {
                return Text(dueDate.formatWithDateOnly);
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onSetDueDateClicked(BuildContext context) async {
    final initialDate = DateTime.now();

    final selectedDateResult = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: initialDate,
      lastDate: initialDate.add(const Duration(days: 365 * 5)),
    );

    if (selectedDateResult == null) return;

    _createTaskController.dueDate = selectedDateResult;
  }
}
