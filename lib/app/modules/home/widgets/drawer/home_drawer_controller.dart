import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:todo/app/services/tasks/tasks_service.dart';
import 'package:todo/app/services/user/user_service.dart';

class HomeDrawerController extends ChangeNotifier {
  final UserService _userService;
  final TasksService _tasksService;

  late final StreamSubscription<User?> _currentUserSubscription;

  bool _isChangingUserName = false;
  bool? _changeUserNameResult;

  HomeDrawerController({
    required UserService userService,
    required TasksService tasksService,
  })  : _userService = userService,
        _tasksService = tasksService {
    _currentUserSubscription =
        _userService.currentUserStream.listen((user) => notifyListeners());
  }

  bool get isChangingUserName => _isChangingUserName;

  bool? get changeUserNameResult => _changeUserNameResult;

  User? get currentUser => _userService.currentUser;

  Future<void> changeUserName(String newName) async {
    _isChangingUserName = true;
    notifyListeners();

    try {
      await _userService.changeUserName(newName);
    } finally {
      _isChangingUserName = false;
      _changeUserNameResult = true;
      notifyListeners();

      _changeUserNameResult = null;
      notifyListeners();
    }
  }

  void signOut() {
    _tasksService.clearAll();
    _userService.signOut();
  }

  @override
  void dispose() {
    _currentUserSubscription.cancel();

    super.dispose();
  }
}
