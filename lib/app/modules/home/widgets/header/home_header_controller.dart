import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:todo/app/services/user/user_service.dart';

class HomeHeaderController extends ChangeNotifier {
  final UserService _userService;

  late final StreamSubscription<User?> _currentUserSubscription;

  HomeHeaderController({
    required UserService userService,
  }) : _userService = userService {
    _currentUserSubscription =
        _userService.currentUserStream.listen((user) => notifyListeners());
  }

  User? get currentUser => _userService.currentUser;

  @override
  void dispose() {
    _currentUserSubscription.cancel();

    super.dispose();
  }
}
