import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:todo/app/core/extensions/theme_extension.dart';
import 'package:todo/app/core/helpers/custom_navigator.dart';
import 'package:todo/app/core/helpers/custom_snack_bar.dart';
import 'package:todo/app/core/ui/custom_icons.dart';
import 'package:todo/app/core/widgets/app_logo.dart';
import 'package:todo/app/core/widgets/custom_text_form_field.dart';
import 'package:todo/app/modules/auth/sign_up/sign_up_controller.dart';

class SignUpPage extends StatefulWidget {
  static const String routeName = '/signUp';

  final SignUpController _signUpController;

  const SignUpPage({
    Key? key,
    required SignUpController signUpController,
  })  : _signUpController = signUpController,
        super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _emailTEC = TextEditingController();
  final TextEditingController _passwordTEC = TextEditingController();
  final TextEditingController _confirmPasswordTEC = TextEditingController();

  @override
  void initState() {
    super.initState();

    widget._signUpController.addListener(_onControllerStateChanged);
  }

  void _onControllerStateChanged() {
    if (widget._signUpController.isSigningUp) {
      Loader.show(context);
    } else {
      Loader.hide();

      widget._signUpController.signUpResult?.fold(
        (errorMessage) => CustomSnackBar(
          type: SnackBarType.error,
          message: errorMessage,
        ).show(context),
        (success) {},
      );
    }
  }

  @override
  void dispose() {
    widget._signUpController.removeListener(_onControllerStateChanged);
    _emailTEC.dispose();
    _passwordTEC.dispose();
    _confirmPasswordTEC.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        leading: IconButton(
          onPressed: () => _onBackClicked(context),
          icon: Icon(
            CustomIcons.back,
            color: context.primaryColor,
          ),
        ),
        title: Text(
          'Sign up',
          style: TextStyle(color: context.primaryColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding:
              const EdgeInsets.symmetric(horizontal: 40).copyWith(bottom: 30),
          child: Column(
            children: [
              const AppLogo(),
              const SizedBox(height: 20),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    CustomTextFormField(
                      controller: _emailTEC,
                      label: 'Email',
                      keyboardType: TextInputType.emailAddress,
                      validator: _validateEmail,
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(height: 20),
                    CustomTextFormField(
                      controller: _passwordTEC,
                      label: 'Password',
                      keyboardType: TextInputType.visiblePassword,
                      validator: _validatePassword,
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(height: 20),
                    CustomTextFormField(
                      controller: _confirmPasswordTEC,
                      label: 'Confirm password',
                      keyboardType: TextInputType.visiblePassword,
                      validator: _validateConfirmPassword,
                      textInputAction: TextInputAction.done,
                    ),
                    const SizedBox(height: 20),
                    Align(
                      alignment: Alignment.centerRight,
                      child: ElevatedButton(
                        onPressed: _onSignUpClicked,
                        child: const Text('Sign up'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onBackClicked(BuildContext context) {
    CustomNavigator.it.pop();
  }

  String? _validateEmail(String? email) {
    return widget._signUpController.validateEmail(email);
  }

  String? _validatePassword(String? password) {
    return widget._signUpController.validatePassword(password);
  }

  String? _validateConfirmPassword(String? confirmPassword) {
    return widget._signUpController.validateConfirmPassword(
      password: _passwordTEC.text,
      confirmPassword: confirmPassword,
    );
  }

  void _onSignUpClicked() {
    final isFormValid = _formKey.currentState?.validate() ?? false;

    if (!isFormValid) return;

    widget._signUpController.signUp(
      email: _emailTEC.text,
      password: _passwordTEC.text,
    );
  }
}
